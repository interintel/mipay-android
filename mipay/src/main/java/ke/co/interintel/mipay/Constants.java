package ke.co.interintel.mipay;

/**
 * Created by danleyb2 on 10/13/17.
 */

public class Constants {
    public static final String MIPAY_APP = "ke.co.interintel.mipay";
    public static final String MIPAY_HOST = "gomipay.com";
}
